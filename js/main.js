$(document).ready(function () {
	$('img.svg').each(function(){
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');

			// Add replaced image's ID to the new SVG
			if (typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if (typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}

			$svg = $svg.removeAttr('xmlns:a');

			$svg = $svg.attr('stroke', '#9bccde');
			$svg = $svg.attr('stroke-width', '0');
			$img.replaceWith($svg);

		}, 'xml');

	});

	$("input[name=phone]").mask("+380 (99) 999-99-99");

	const $checkBox = $('.checkbox');
	const dataSend = {};
	let counter = 0;
	$checkBox.each((index, item) => {
		counter += 300;
		setTimeout(() => {
			$(item).find('.checkbox__box').addClass('active');
		}, counter);
	});

	const validation = (input) => {
		if (input.value === '') {
			input.classList.add('error');
		} else {
			input.classList.remove('error');
		}
	};

	$('.form-box').on('submit', function (e) {
		e.preventDefault();
		let $fromInputs = $(this).find('input');
		let dataUrl = $(this).attr('action');
		$fromInputs.each((index, item) => {
			validation(item);
			dataSend[$(item).attr('name')] = $(item).val();
		});
		if (!$fromInputs.hasClass('error')) {
			$.ajax({
				url: dataUrl,
				method: "POST",
				data: dataSend,
			})
				.done(function (res) {
					if (res.status === 'success') {
						$('#modalSuccess').modal('show');
					}
				})
				.fail(function (res) {
					console.log(res);
				})
		}
	});
});