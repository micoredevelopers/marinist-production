<?php

include 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['swiftmailer.options'] = array(
    'host' => 'smtp.gmail.com',
    'port' => '587',
    'username' => 'Micoretest@gmail.com',
    'password' => 'Micore123',
    'encryption' => 'tls',
    'auth_mode' => null
);
//$app['swiftmailer.transport'] = new Swift_SendmailTransport();
//$app['swiftmailer.use_spool'] = false;
$app['app.server_email'] = 'Micoretest@gmail.com';
$app['app.admin_email'] = 'oshchyp.denys@gmail.com';

$app->post('/feedback', function (Request $request) use ($app) {

    $data = [
        'name' => (string)$request->get('name'),
        'phone' => (string)$request->get('phone')
    ];

    $mailBody = file_get_contents('emails/feedback.html');
    foreach ($data as $k=>$val){
        $mailBody = str_replace('{'.$k.'}',$val, $mailBody);
    }

    $message = new \Swift_Message();
    $message
        ->setSubject('Feedback')
        ->setFrom($app['app.server_email'])
        ->setTo($app['app.admin_email'])
        ->setBody($mailBody, 'text/html');

    $app['mailer']->send($message);
    return $app->json(['status' => 'success']);
});

$app->get('/', function (){
    return file_get_contents('index.html');
});

$app->run();